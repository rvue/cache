export { default as cache } from "./bin/cache";

export default {
  install(Vue, options) {
    Vue.use(cache);
  }
};
