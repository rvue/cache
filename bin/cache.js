import Vue from "vue";

Vue.cache = {
  _config: {
    id: "container",
    is: "div"
  },
  store: {}
};

export default {
  data() {
    if (!this.$options.cache) return {};
    let isCacheString = typeof this.$options.cache === "string";
    let config = isCacheString
      ? { id: this.$options.cache }
      : this.$options.cache;
    return {
      cache_config: { ...Vue.cache._config, ...config },
      canCache: true
    };
  },
  computed: {
    cached() {
      if (!this.canCache) return {};
      return !!Vue.cache.store[this.cache_config.id];
    },
    element() {
      if (!this.canCache) return {};
      return document.getElementById(this.cache_config.id);
    }
  },
  mounted() {
    if (!this.canCache) return;

    if (!this.cached) {
      Vue.cache.store[this.cache_config.id] = this.element;
    } else {
      let cachedRef = Vue.cache.store[this.cache_config.id];
      this.element.parentNode.replaceChild(cachedRef, this.element);
    }
  },
  render(h) {
    if (!this.canCache) return null;
    return h(this.cache_config.is, {
      attrs: {
        id: this.cache_config.id
      },
      class: this.cache_config.class
    });
  }
};
