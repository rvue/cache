# Vue Cache Mixin

This mixin will allow you to cache dom elements.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Install

Yarn

```
yarn add rvue-mixin-cache
```

NPM

```
npm install rvue-mixin-cache
```

PNPM

```
pnpm install rvue-mixin-cache
```

---

### Usage

- Import the mixin
- Add it to the mixins property of a component
- Specify the cache configuration via the cache property of a component.

This mixin is designed for single renderless components (it renders the element itself).

---

### Example

```
import CacheMixin from 'rvue-mixin-cache'

export default {
  ...
  mixins: [CacheMixin],
  cache: <Configuration>
  ...
}
```

---

### Configuration

Configuration can either be an object or a string containing a unique value.

```
	cache: 'unique-value'
```

```
	cache: {
		id: 'unique-value',
		class: '...',
		is: 'div',
	}
```

---

### Inherited Computed Properties

```
cached: <boolean>

element: <the current dom element>
```

## License

This project is licensed under the ISC License - see the [LICENSE.md](LICENSE.md) file for details
